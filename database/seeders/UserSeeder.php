<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Teknisi',
            'email' => 'teknisi@gmail.com',
            'password' => Hash::make('teknisi123'),
        ]);

        $user->assignRole('Teknisi');

        $user = User::create([
            'name' => 'SPV',
            'email' => 'spv@gmail.com',
            'password' => Hash::make('spv123'),
        ]);

        $user->assignRole('SPV');

        $user = User::create([
            'name' => 'Manager',
            'email' => 'manager@gmail.com',
            'password' => Hash::make('manager123'),
        ]);

        $user->assignRole('Manager');

        $user = User::create([
            'name' => 'Admin Finance',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin123'),
        ]);

        $user->assignRole('Admin Finance');

        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'super@gmail.com',
            'password' => Hash::make('super123'),
        ]);

        $user->assignRole('Super Admin');
    }
}
