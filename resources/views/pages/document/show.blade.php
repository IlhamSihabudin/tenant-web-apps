@extends('layouts.master')
@section('title', 'Detail Document')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Detail</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
            <a href="" class="text-muted">Document</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">Detail</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Mixed Widget 10-->
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">{{ $data->name_tenant }}</h3>
                </div>
            </div>

            <div class="card-body">
                <div class="container">
                    <form action="" method="POST" id="formApproval">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <table width="60%">
                                    <tr>
                                        <td>Periode Pemakaian</td>
                                        <td>:</td>
                                        <td>{{ date('d F Y', strtotime($data->start_period)) . ' s/d ' . date('d F Y', strtotime($data->end_period)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tenant PIC</td>
                                        <td>:</td>
                                        <td>{{ $data->tenant_pic }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row mt-10">
                            <div class="col-12">
                                <h5>Detail Listrik</h5>
                            </div>
                            @foreach ($data->documentDetailListrik as $listrik)
                            <div class="col-md-6 col-12 mt-4">
                                <div class="card card-custom border border-primary">
                                    <div class="card-body">
                                        <table width="100%">
                                            <tr>
                                                <td width="40%">Nama Panel</td>
                                                <td>:</td>
                                                <td>{{ $listrik->name_panel }}</td>
                                            </tr>
                                            <tr>
                                                <td>Pencatatan Meteran</td>
                                                <td>:</td>
                                                <td>{{ $listrik->pencatatan_meteran }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">Photo</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <img src="{{ asset('storage/listrik_detail/'.$listrik->photo) }}" alt="{{ $listrik->photo }}" width="100%">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="row mt-10">
                            <div class="col-12">
                                <h5>Detail Air</h5>
                            </div>
                            @foreach ($data->documentDetailAir as $air)
                            <div class="col-md-6 col-12 mt-4">
                                <div class="card card-custom border border-primary">
                                    <div class="card-body">
                                        <table width="100%">
                                            <tr>
                                                <td width="40%">Lokasi</td>
                                                <td>:</td>
                                                <td>{{ $air->lokasi }}</td>
                                            </tr>
                                            <tr>
                                                <td>No. Seri</td>
                                                <td>:</td>
                                                <td>{{ $air->no_seri }}</td>
                                            </tr>
                                            <tr>
                                                <td>Pencatatan Meteran</td>
                                                <td>:</td>
                                                <td>{{ $air->pencatatan_meteran }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">Photo</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <img src="{{ asset('storage/air_detail/'.$air->photo) }}" alt="{{ $air->photo }}" width="100%">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="row mt-10">
                            <div class="col-md-6 col-12 mt-4">
                                <h5>Notes</h5>
                                <textarea class="form-control" name="notes" id="notes" rows="5"></textarea>
                            </div>
                            <div class="col-md-6 col-12 mt-4">
                                <h5>TTD Tenant</h5>
                                <img src="{{ asset('storage/signature/'.$data->signature) }}" alt="{{ $data->signature }}">
                            </div>
                        </div>
                        @if ((Auth::user()->hasRole('SPV') && $data->status == 1) || (Auth::user()->hasRole('Manager') && $data->status == 2))
                        <div class="row mt-10">
                            <div class="col-md col-12 mt-4">
                                <button type="button" onclick="approvalSubmitAction('{{ route('document.reject', $data) }}')" class="btn btn-danger btn-block">Reject</button>
                            </div>
                            <div class="col-md col-12 mt-4">
                                <button type="button" onclick="approvalSubmitAction('{{ route('document.approve', $data) }}')" class="btn btn-success btn-block">Approve</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        <!--end::Mixed Widget 10-->
    </div>
</div>
@endsection

@push('js-script')
<script>
    function approvalSubmitAction($url) {
        $('#formApproval').attr('action', $url);
        $('#formApproval').submit();
    }
</script>
@endpush
