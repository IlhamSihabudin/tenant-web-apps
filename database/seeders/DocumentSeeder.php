<?php

namespace Database\Seeders;

use App\Models\Document;
use App\Models\DocumentApproval;
use App\Models\DocumentDetailAir;
use App\Models\DocumentDetailListrik;
use App\Models\Role;
use Illuminate\Database\Seeder;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $document = Document::create([
            'name_tenant'   => "PT. ABC",
            'start_period'  => "2022-07-01",
            'end_period'    => "2022-08-01",
            'tenant_pic'    => "Budi",
            'signature'     => "signature.jpg",
            'status'        => 1,
            'created_by'    => 1,
        ]);

        DocumentDetailListrik::create([
            'document_id' => $document->id,
            'name_panel' => "Panel 1",
            'pencatatan_meteran' => 50,
            'photo' => "photo_listrik.jpg",
        ]);

        DocumentDetailAir::create([
            'document_id' => $document->id,
            'lokasi' => "Bogor",
            'no_seri' => "001",
            'pencatatan_meteran' => 50,
            'photo' => "photo_air.jpg",
        ]);

        $documentApproval = DocumentApproval::create([
            'document_id' => $document->id,
            'user_id' => 1,
            'position' => Role::where('name', 'Teknisi')->first()->name,
            'date' => date('Y-m-d H:i:s'),
            'message' => 'Document Created',
            'status' => 1,
        ]);
    }
}
