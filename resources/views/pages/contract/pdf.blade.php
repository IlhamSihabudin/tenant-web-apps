<!doctype html>
<html lang="en">
<head>
    <title>Document PDF</title>

    <style>
        body{
            font-size: 11px;
        }

        .container{
            /*margin: 10px;*/
            padding: 0 20px 20px 20px;
            /*border: 1px solid black;*/
        }

        table, tr {
            page-break-inside: auto;
            border-collapse: collapse;
            vertical-align: middle;
        }

        td {
            border-collapse: collapse;
            vertical-align: middle;
            padding: 5px 3px;
        }

        .mt-1{
            margin-top: 10px;
            margin-bottom: 20px;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>
<body>
<div class="container">
    <table>
        <tr>
            <td style="font-size: 18px">
                <b>{{ $document->name_tenant }}</b><br>
            </td>
        </tr>
    </table>
    <hr>
    <table width="60%">
        <tr>
            <td>Periode Pemakaian</td>
            <td>:</td>
            <td>{{ date('d F Y', strtotime($document->start_period)) . ' s/d ' . date('d F Y', strtotime($document->end_period)) }}</td>
        </tr>
        <tr>
            <td>Tenant PIC</td>
            <td>:</td>
            <td>{{ $document->tenant_pic }}</td>
        </tr>
    </table>
    <div style="margin-top: 20px">
        <h2>Detail Listrik</h2>
    </div>
    <table width="100%" border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Panel</th>
                <th>Pencatatan Meteran</th>
        </thead>
        <tbody>
            @foreach ($document->documentDetailListrik as $listrik)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $listrik->name_panel }}</td>
                    <td>{{ $listrik->pencatatan_meteran }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div style="margin-top: 20px">
        <h2>Detail Air</h2>
    </div>
    <table width="100%" border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>Lokasi</th>
                <th>No. Seri</th>
                <th>Pencatatan Meteran</th>
        </thead>
        <tbody>
            @foreach ($document->documentDetailAir as $air)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $air->lokasi }}</td>
                    <td>{{ $air->no_seri }}</td>
                    <td>{{ $air->pencatatan_meteran }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div style="margin-top: 50px;margin-right: 20px;margin-bottom: 20px;float:right">
        <h2>Signed By,</h2>
        <img src="{{ asset('storage/signature/'.$document->signature) }}" alt="$document->signature">
    </div>
    <hr style="clear: both;margin: 20px 0;border: 1px dashed black;">
    <div style="clear: both;display: block">
        <h2>Detail Listrik Attachment</h2>
        <div style="width: 100%;">
            @foreach($document->documentDetailListrik as $listrik)
                <div style="padding: 0 5px;float: left;">
                    <img src="{{ asset('storage/listrik_detail/'.$listrik->photo) }}" class="mt-1" alt="{{ $listrik->photo }}" style="max-width: 80%; max-height: 300px">
                </div>
            @endforeach
        </div>
    </div>
    <div style="clear: both;display: block">
        <h2 style="margin-top: 20px">Detail Air Attachment</h2>
        <div style="width: 100%;">
            @foreach($document->documentDetailAir as $air)
                <div style="padding: 0 5px;float: left;">
                    <img src="{{ asset('storage/air_detail/'.$air->photo) }}" class="mt-1" alt="{{ $air->photo }}" style="max-width: 80%; max-height: 300px">
                </div>
            @endforeach
        </div>
    </div>
</div>
</body>
</html>
