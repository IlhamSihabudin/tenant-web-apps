<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        return view('pages.profile.index');
    }

    public function update(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email'  => 'required',
        ]);

        DB::beginTransaction();

        try {
            $user = Auth::user();

            $user->update([
                'name'      => $request->name,
                'email'     => $request->email,
            ]);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors($e->getMessage())->withInput();
        }

        if (isset($request->password) && !empty($request->password)){
            $request->validate([
                'password'  => 'required|min:6|confirmed'
            ]);

            $user->update([
                'password'  => Hash::make($request->password)
            ]);
        }

        DB::commit();
        return back()->withMessage('Update profile successfully');
    }
}
