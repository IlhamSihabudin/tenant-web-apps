<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Models\Document;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/pdf', function () {
    return view('pages.contract.pdf', [
        'document' => Document::first()
    ]);
});

Auth::routes();

Route::group(['middleware' => ['auth']], function (){
    // User
    Route::resource('user', UserController::class);

    // Document
    Route::get('/document', [App\Http\Controllers\DocumentController::class, 'index'])->name('document.index');
    Route::get('/document/{document}', [App\Http\Controllers\DocumentController::class, 'show'])->name('document.show');
    Route::post('/document/{document}/approve', [App\Http\Controllers\DocumentController::class, 'approve'])->name('document.approve');
    Route::post('/document/{document}/reject', [App\Http\Controllers\DocumentController::class, 'reject'])->name('document.reject');

    // History
    Route::get('/history', [App\Http\Controllers\HistoryController::class, 'index'])->name('history.index');
    Route::get('/history/{document}', [App\Http\Controllers\HistoryController::class, 'show'])->name('history.show');

    // Contract
    Route::get('/contract', [App\Http\Controllers\ContractController::class, 'index'])->name('contract.index');
    Route::get('/contract/{document}', [App\Http\Controllers\ContractController::class, 'show'])->name('contract.show');
    Route::get('/contract/{document}/pdf', [App\Http\Controllers\ContractController::class, 'exportPdf'])->name('contract.pdf');

    // Profile
    Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile.index');
    Route::post('/profile', [App\Http\Controllers\ProfileController::class, 'update'])->name('profile.update');
});

// Dashboard
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
