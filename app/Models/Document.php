<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function documentDetailAir()
    {
        return $this->hasMany(DocumentDetailAir::class, 'document_id', 'id');
    }

    public function documentDetailListrik()
    {
        return $this->hasMany(DocumentDetailListrik::class, 'document_id', 'id');
    }

    public function documentApproval()
    {
        return $this->hasMany(DocumentApproval::class, 'document_id', 'id');
    }

    public function documentLastApproval() {
        return $this->hasOne(DocumentApproval::class, 'document_id', 'id')->latest();
    }
}
