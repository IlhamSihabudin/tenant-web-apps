<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\DocumentApproval;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DocumentController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()){
            $query = Document::query();

            if (Auth::user()->hasRole('SPV')) {
                $query->where('status', '>=', 1);
            }elseif (Auth::user()->hasRole('Manager')) {
                $query->where('status', '>=', 2);
            }elseif (Auth::user()->hasRole('Admin Finance')) {
                $query->where('status', '>=', 3);
            }

            return DataTables::of($query)
                ->addColumn('periode', function ($model) {
                    return date('d F Y', strtotime($model->start_period)) . ' s/d ' . date('d F Y', strtotime($model->end_period));
                })
                ->editColumn('created_at', function ($model) {
                    return date('H:i d/m/Y', strtotime($model->created_at));
                })
                ->editColumn('status', function ($model) {
                    $response = "";
                    if ($model->status == 0) {
                        $response = "<span class='badge badge-danger'>Rejected</span>";
                    } elseif ($model->status == 1) {
                        $response = "<span class='badge badge-warning'>Document Created</span>";
                    } elseif ($model->status == 2) {
                        $response = "<span class='badge badge-success'>SPV Approved</span>";
                    } elseif ($model->status == 3) {
                        $response = "<span class='badge badge-primary'>Document Finised</span>";
                    }

                    return $response;
                })
                ->editColumn('action', function ($model) {
                    $response = "<div class='text-center'>";

                    $btn_approve = 0;
                    if (Auth::user()->hasRole('SPV') && $model->status == 1) {
                        $btn_approve = 1;
                    }elseif (Auth::user()->hasRole('Manager') && $model->status == 2) {
                        $btn_approve = 1;
                    }

                    if ($btn_approve == 1) {
                        $response .= "
                            <button onclick=\"approvalSubmitAction('".route('document.approve', $model['id'])."')\" class='btn btn-sm btn-light-success btn-circle btn-icon mr-2' title='Approve'><i class='far fa-thumbs-up icon-nm'></i></button>
                            <button onclick=\"approvalSubmitAction('".route('document.reject', $model['id'])."')\" class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2' title='Reject'><i class='far fa-thumbs-down icon-nm'></i></button>
                        ";
                    }

                    $response .= "<a href='".route('document.show', $model['id'])."' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2' title='Detail'><i class='fas fa-list icon-nm'></i></a>
                        </div>";

                    return $response;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }

        return view('pages.document.index');
    }

    public function show(Document $document)
    {
        return view('pages.document.show', [
            'data' => $document
        ]);
    }

    public function approve(Document $document)
    {
        try {
            DB::beginTransaction();
            $documentApproval = DocumentApproval::create([
                'document_id' => $document->id,
                'user_id' => Auth::guard('sanctum')->user()->id,
                'position' => Auth::guard('sanctum')->user()->getRoleNames()[0],
                'date' => date('Y-m-d H:i:s'),
                'message' => 'Document Approved by '.Auth::guard('sanctum')->user()->getRoleNames()[0],
                'status' => $document->status + 1,
            ]);

            $document->update([
                'status' => $document->status + 1
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

        DB::commit();
        return redirect()->route('document.index')->withMessage('Document Approved');
    }

    public function reject(Document $document)
    {
        try {
            DB::beginTransaction();
            $documentApproval = DocumentApproval::create([
                'document_id' => $document->id,
                'user_id' => Auth::guard('sanctum')->user()->id,
                'position' => Auth::guard('sanctum')->user()->getRoleNames()[0],
                'date' => date('Y-m-d H:i:s'),
                'message' => 'Document Approved by '.Auth::guard('sanctum')->user()->getRoleNames()[0],
                'status' => 0,
            ]);

            $document->update([
                'status' => 0
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

        DB::commit();
        return redirect()->route('document.index')->withMessage('Document Rejected');
    }
}
