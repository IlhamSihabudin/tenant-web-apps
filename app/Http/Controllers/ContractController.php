<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
use PDF;

class ContractController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()){
            $query = Document::query();

            if (Auth::user()->hasRole('SPV')) {
                $query->where('status', '>=', 1);
            }elseif (Auth::user()->hasRole('Manager')) {
                $query->where('status', '>=', 2);
            }elseif (Auth::user()->hasRole('Admin Finance')) {
                $query->where('status', '>=', 3);
            }

            return DataTables::of($query)
                ->addColumn('periode', function ($model) {
                    return date('d F Y', strtotime($model->start_period)) . ' s/d ' . date('d F Y', strtotime($model->end_period));
                })
                ->editColumn('created_at', function ($model) {
                    return date('H:i d/m/Y', strtotime($model->created_at));
                })
                ->editColumn('status', function ($model) {
                    $response = "";
                    if ($model->status == 0) {
                        $response = "<span class='badge badge-danger'>Rejected</span>";
                    } elseif ($model->status == 1) {
                        $response = "<span class='badge badge-warning'>Document Created</span>";
                    } elseif ($model->status == 2) {
                        $response = "<span class='badge badge-success'>SPV Approved</span>";
                    } elseif ($model->status == 3) {
                        $response = "<span class='badge badge-primary'>Document Finised</span>";
                    }

                    return $response;
                })
                ->editColumn('action', function ($model) {
                    $response = "<div class='text-center'>
                                    <a href='".route('contract.show', $model['id'])."' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2' title='Detail'><i class='fas fa-list icon-nm'></i></a>
                                    <a href='".route('contract.pdf', $model['id'])."' class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2' title='Export PDF'><i class='fas fa-file-pdf icon-nm'></i></a>
                                </div>";

                    return $response;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }

        return view('pages.contract.index');
    }

    public function show(Document $document)
    {
        return view('pages.contract.show', [
            'data' => $document
        ]);
    }

    public function exportPdf(Document $document)
    {
        $pdf = PDF::loadView('pages.contract.pdf', compact('document'));
        return $pdf->download($document->name_tenant.'_'.date('YmdHis').'.pdf');
    }
}
