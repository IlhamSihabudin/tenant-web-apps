<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function queryDocument(){
        $query = Document::query();

            if (Auth::user()->hasRole('SPV')) {
                $query->where('status', '>=', 1);
            }elseif (Auth::user()->hasRole('Manager')) {
                $query->where('status', '>=', 2);
            }elseif (Auth::user()->hasRole('Admin Finance')) {
                $query->where('status', '>=', 3);
            }

        return $query;
    }

    public function index()
    {
        $period = now()->subMonths(4)->monthsUntil(now());

        $header = [];
        $chart = [];
        foreach ($period as $date)
        {
            $header[] = $date->monthName . " " . $date->year;
            $chart[] = $this->queryDocument()->where('created_at', 'LIKE', $date->year.'-'.sprintf("%02d", $date->month).'%')->count('id');
        }

        $doc_count = [
            'doc_on_progress'   => $this->queryDocument()->where('status', '!=', '3')->count('id'),
            'doc_finished'      => $this->queryDocument()->where('status', '3')->count('id'),
        ];

        $datas = [
            'doc_total' => $this->queryDocument()->count('id'),
            'doc_count' => $doc_count,
            'chart' => [
                'header' => $header,
                'value' => $chart
            ]
        ];

        return view('home', $datas);
    }
}
