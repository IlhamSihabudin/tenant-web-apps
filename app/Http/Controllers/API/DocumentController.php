<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\DocumentApproval;
use App\Models\DocumentDetailAir;
use App\Models\DocumentDetailListrik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::where('created_by', Auth::user()->id)
            ->with('documentLastApproval')
            ->get();
        return response()->json([
            'message' => 'Document List Success',
            'data' => $documents,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $validation = Validator::make($request->all(),[
                'name_tenant' => 'required',
                'start_period' => 'required|date',
                'end_period' => 'required|date',
                'tenant_pic' => 'required',
                'signature' => 'required',
                'detail_listrik' => 'required|array',
                'detail_air' => 'required|array',
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'message' => $validation->messages(),
                    'data' => [],
                ], 500);
            } else {
                $signature = "";
                if ($request->hasFile('signature')) {
                    $signature = date('YmdHis') . '_' . Auth::guard('sanctum')->user()->name . '.' . $request->file('signature')->getClientOriginalExtension();
                    Storage::disk('public')->putFileAs('signature', $request->file('signature'), $signature);
                }

                $document = Document::create([
                    'name_tenant'   => $request->name_tenant,
                    'start_period'  => $request->start_period,
                    'end_period'    => $request->end_period,
                    'tenant_pic'    => $request->tenant_pic,
                    'signature'     => $signature,
                    'status'        => 1,
                    'created_by'    => Auth::guard('sanctum')->user()->id,
                ]);
    
                if (!empty($request->detail_listrik)) {
                    foreach ($request->detail_listrik as $item) {
                        $photo = "";
                        if ($item['photo']) {
                            $photo = date('YmdHis') . '_' . $document->id . '.' . $item['photo']->getClientOriginalExtension();
                            Storage::disk('public')->putFileAs('listrik_detail', $item['photo'], $signature);
                        }
    
                        DocumentDetailListrik::create([
                            'document_id' => $document->id,
                            'name_panel' => $item['name_panel'],
                            'pencatatan_meteran' => $item['pencatatan_meteran'],
                            'photo' => $photo,
                        ]);
                    }
                }
    
                if (!empty($request->detail_air)) {
                    foreach ($request->detail_air as $item) {
                        $photo = "";
                        if ($item['photo']) {
                            $photo = date('YmdHis') . '_' . $document->id . '.' . $item['photo']->getClientOriginalExtension();
                            Storage::disk('public')->putFileAs('air_detail', $item['photo'], $signature);
                        }
    
                        DocumentDetailAir::create([
                            'document_id' => $document->id,
                            'lokasi' => $item['lokasi'],
                            'no_seri' => $item['no_seri'],
                            'pencatatan_meteran' => $item['pencatatan_meteran'],
                            'photo' => $item['photo'],
                        ]);
                    }
                }
    
                $documentApproval = DocumentApproval::create([
                    'document_id' => $document->id,
                    'user_id' => Auth::guard('sanctum')->user()->id,
                    'position' => Auth::guard('sanctum')->user()->getRoleNames()[0],
                    'date' => date('Y-m-d H:i:s'),
                    'message' => 'Document Created',
                    'status' => 1,
                ]);
    
                if (!$document && !$documentApproval) {
                    DB::rollBack();
                    return response()->json([
                        'message' => 'Create Document Failed',
                        'data' => [],
                    ], 400);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'data' => [],
            ], 500);
        }

        DB::commit();
        return response()->json([
            'message' => 'Create Document Successfully',
            'data' => $document,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        try {
            $document->documentDetailListrik;
            $document->documentDetailAir;
            $document->document_approval = $document->documentApproval()->orderBy('created_at', 'asc')->get();
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Document Not Found',
                'data' => [],
            ], 400);
        }

        return response()->json([
            'message' => 'Show Document Detail Success',
            'data' => $document,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        try {
            DB::beginTransaction();

            $validation = Validator::make($request->all(),[
                'name_tenant' => 'required',
                'start_period' => 'required|date',
                'end_period' => 'required|date',
                'tenant_pic' => 'required',
                'signature' => 'required',
                'detail_listrik' => 'required|array',
                'detail_air' => 'required|array',
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'message' => $validation->messages(),
                    'data' => [],
                ], 500);
            } else {
                $signature = "";
                if ($request->hasFile('signature')) {
                    $signature = date('YmdHis') . '_' . Auth::guard('sanctum')->user()->name . '.' . $request->file('signature')->getClientOriginalExtension();
                    Storage::disk('public')->putFileAs('signature', $request->file('signature'), $signature);
                }
    
                $document->update([
                    'name_tenant'   => $request->name_tenant,
                    'start_period'  => $request->start_period,
                    'end_period'    => $request->end_period,
                    'tenant_pic'    => $request->tenant_pic,
                    'signature'     => $signature,
                    'status'        => 1,
                    'created_by'    => Auth::guard('sanctum')->user()->id,
                ]);
    
                DocumentDetailListrik::where('document_id', $document->id)->delete();
                if (!empty($request->detail_listrik)) {
                    foreach ($request->detail_listrik as $item) {
                        $photo = "";
                        if ($item['photo']) {
                            $photo = date('YmdHis') . '_' . $document->id . '.' . $item['photo']->getClientOriginalExtension();
                            Storage::disk('public')->putFileAs('listrik_detail', $item['photo'], $signature);
                        }
    
                        DocumentDetailListrik::create([
                            'document_id' => $document->id,
                            'name_panel' => $item['name_panel'],
                            'pencatatan_meteran' => $item['pencatatan_meteran'],
                            'photo' => $photo,
                        ]);
                    }
                }
    
                DocumentDetailAir::where('document_id', $document->id)->delete();
                if (!empty($request->detail_air)) {
                    foreach ($request->detail_air as $item) {
                        $photo = "";
                        if ($item['photo']) {
                            $photo = date('YmdHis') . '_' . $document->id . '.' . $item['photo']->getClientOriginalExtension();
                            Storage::disk('public')->putFileAs('air_detail', $item['photo'], $signature);
                        }
    
                        DocumentDetailAir::create([
                            'document_id' => $document->id,
                            'lokasi' => $item['lokasi'],
                            'no_seri' => $item['no_seri'],
                            'pencatatan_meteran' => $item['pencatatan_meteran'],
                            'photo' => $item['photo'],
                        ]);
                    }
                }
    
                $documentApproval = DocumentApproval::create([
                    'document_id' => $document->id,
                    'user_id' => Auth::guard('sanctum')->user()->id,
                    'position' => Auth::guard('sanctum')->user()->getRoleNames()[0],
                    'date' => date('Y-m-d H:i:s'),
                    'message' => 'Document Resubmitted',
                    'status' => 1,
                ]);
    
                if (!$document && !$documentApproval) {
                    DB::rollBack();
                    return response()->json([
                        'message' => 'Create Document Failed',
                        'data' => [],
                    ], 400);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'data' => [],
            ], 500);
        }

        DB::commit();
        return response()->json([
            'message' => 'Create Document Successfully',
            'data' => $document,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        //
    }
}
