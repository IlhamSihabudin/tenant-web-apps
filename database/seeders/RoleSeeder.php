<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'Teknisi']);
        Role::create(['name' => 'SPV']);
        Role::create(['name' => 'Manager']);
        Role::create(['name' => 'Admin Finance']);
        Role::create(['name' => 'Super Admin']);
    }
}
